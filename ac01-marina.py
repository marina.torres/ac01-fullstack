from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/cores', methods=['POST'])
def ordenar_cores():
    cores = request.json['cores']
    cores_ordenadas = sorted(cores)
    return jsonify({'cores_ordenadas': cores_ordenadas})

if __name__ == '__main__':
    app.run(debug=True)


#http://localhost:5000/cores